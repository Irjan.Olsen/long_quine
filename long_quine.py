import sys

i = 0

f = open("Hello_From_Irjan_Olsen_nr_"+str(i)+".txt", "w+")
f.write("Hello! Iteration nr. " + str(i))
f.close()

quine = """
import sys

i = %i

f = open("Hello_From_Irjan_Olsen_nr_"+str(i)+".txt", "w+")
f.write("Hello! Iteration nr. " + str(i))
f.close()

quine = %s

sys.stdout.write(quine%%(i+1, '"'*3+quine+'"'*3))
"""
sys.stdout.write(quine%(i+1, '"'*3+quine+'"'*3))
